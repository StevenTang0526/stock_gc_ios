//
//  AuthManager.swift
//  SPAPP
//
//  Created by LaurenceRedpay20 on 2020/7/31.
//  Copyright © 2020 REDPAY - Laurence. All rights reserved.
//

import Foundation

class DefaultManager {
    
    static let sharedInstance = DefaultManager()
    private init() {}
    
    var walletCurrencyCount: Int {
        get { return UserDefaults.standard.integer(forKey: "walletCurrencyCount") }
        set { UserDefaults.standard.set(newValue, forKey: "walletCurrencyCount") }
    }
    
    var token: String {
        get { return UserDefaults.standard.string(forKey: "token") ?? "" }
        set { UserDefaults.standard.set("Bearer "+newValue, forKey: "token") }
    }
    
    var wallet: [String : String] {
        get { return UserDefaults.standard.value(forKey: "walletDict") as? [String : String] ?? [:] }
        set { UserDefaults.setValue(newValue, forKey: "walletDict") }
    }
    
    // Hui: 記錄使用者是否已經手動備份，如果已備份就不在 wallet home 頁顯示警告
    var walletUserIsBackuped: Bool {
        get { return UserDefaults.standard.bool(forKey: "walletUserIsBackuped") }
        set { UserDefaults.standard.set(newValue, forKey: "walletUserIsBackuped") }
    }
    
    var userAcct: String {
        get { return UserDefaults.standard.string(forKey: "userAcct") ?? "" }
        set { UserDefaults.standard.set(newValue, forKey: "userAcct") }
    }
    
    func clearAllUserDefaultsData() {
        let userDefaults = UserDefaults.standard
        let dics = userDefaults.dictionaryRepresentation()
        for key in dics {
            userDefaults.removeObject(forKey: key.key)
            
        }
        userDefaults.synchronize()
    }
    
}

