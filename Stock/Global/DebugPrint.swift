//
//  DebugPrint.swift
//  SPAPP
//
//  Created by REDPAY - Laurence on 2020/6/16.
//  Copyright © 2020 REDPAY - Laurence. All rights reserved.
//

import Foundation

func dPrint(_ item: Any..., function: String = #function) {
    #if DEBUG
        print("Check@ \(function): \(item)")
    #endif
}

func aPrint(_ item: Any..., function: String = #function) {
    #if DEBUG
        print("*---Andrew---*: \(item)")
    #endif
}
