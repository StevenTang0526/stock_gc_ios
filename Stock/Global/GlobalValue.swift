//
//  GlobalValue.swift
//  SPAPP
//
//  Created by REDPAY - Laurence on 2020/6/9.
//  Copyright © 2020 REDPAY - Laurence. All rights reserved.
//

import Foundation
import UIKit
import DeviceKit

enum GlobalValue {
    static let uuid = UIDevice.current.identifierForVendor?.uuidString ?? "uuid"
    static let osVersion = UIDevice.current.systemVersion
    static let screenWidth = UIScreen.main.bounds.width
    static let screenHeight = UIScreen.main.bounds.height
    static let currentDevice = Device.current
    static var fcmToken = ""
    static var appearceMode: AppearceMode = .light
    static var token = ""
    static var avatorImage = UIImage(named: "avator")//future need to be base64
    static var forgotPasswordId = ""
    static var verificationId = ""
    static var verifyCheckId = ""
    static var thisYear = GlobalFunction.getYear()
    static var thisMonth = GlobalFunction.getMonth()
    static var thisDay = GlobalFunction.getDay()
    static var acctName = ""
    static var dynamicURL = ""
    static var isfromDeeplink = false
}


enum AppearceMode:Int {
    case dark = 0
    case light = 1
}

enum RewardType: Int {
    case car = 1
    case event = 2
    case award = 3
    case survey = 4
    case health = 5
    case ticket = 6
}

struct GlobalFunction {
    static func getYear() -> Int {
        let date: Date = Date()
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        dateFormatter.locale = Locale(identifier: "zh_Hant_TW") // 設定地區(台灣)
        dateFormatter.timeZone = TimeZone(identifier: "Asia/Taipei") // 設定時區(台灣)
        let dateFormatString: String = dateFormatter.string(from: date)
        return Int(dateFormatString)!
    }
    
    static func getMonth() -> Int {
        let date: Date = Date()
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM"
        dateFormatter.locale = Locale(identifier: "zh_Hant_TW") // 設定地區(台灣)
        dateFormatter.timeZone = TimeZone(identifier: "Asia/Taipei") // 設定時區(台灣)
        let dateFormatString: String = dateFormatter.string(from: date)
        return Int(dateFormatString)!
    }
    
    static func getDay() -> Int {
        let date: Date = Date()
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd"
        dateFormatter.locale = Locale(identifier: "zh_Hant_TW") // 設定地區(台灣)
        dateFormatter.timeZone = TimeZone(identifier: "Asia/Taipei") // 設定時區(台灣)
        let dateFormatString: String = dateFormatter.string(from: date)
        return Int(dateFormatString)!
    }
    
    static func getSysDateStr() -> String {
        let date: Date = Date()
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd HH"
        dateFormatter.locale = Locale(identifier: "zh_Hant_TW") // 設定地區(台灣)
        dateFormatter.timeZone = TimeZone(identifier: "Asia/Taipei") // 設定時區(台灣)
        let dateFormatString: String = dateFormatter.string(from: date)
        return dateFormatString
    }
    
    static func getSysDate() -> Date {
        let date: Date = Date()
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd HH"
        dateFormatter.locale = Locale(identifier: "zh_Hant_TW") // 設定地區(台灣)
        dateFormatter.timeZone = TimeZone(identifier: "Asia/Taipei") // 設定時區(台灣)
        let dateFormatString: String = dateFormatter.string(from: date)
        return dateFormatter.date(from: dateFormatString)!
    }
    
    static func getDateFromString(withFormat strFormat: String, strDate: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = strFormat
        dateFormatter.locale = Locale(identifier: "zh_Hant_TW") // 設定地區(台灣)
        dateFormatter.timeZone = TimeZone(identifier: "Asia/Taipei") // 設定時區(台灣)
        return dateFormatter.date(from: strDate)!
    }
}




