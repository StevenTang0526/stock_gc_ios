//
//  UIColorExtension.swift
//  SPAPP
//
//  Created by REDPAY - Laurence on 2020/6/9.
//  Copyright © 2020 REDPAY - Laurence. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {

    static let riseRed = #colorLiteral(red: 0.8862745098, green: 0.1137254902, blue: 0.3294117647, alpha: 1)
    static let riseRed40 = #colorLiteral(red: 0.8862745098, green: 0.1137254902, blue: 0.3294117647, alpha: 0.401567851)
    static let riseBlack = #colorLiteral(red: 0.02352941176, green: 0.02352941176, blue: 0.02352941176, alpha: 1)
    static let riseDisableGray = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.2522206764)
    static let riseBlack20 = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1980682791)
    static let riseBlack25 = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.2549497003)
    static let riseBlack40 = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.401567851)
    static let riseBlack50 = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)
    static let riseBlack60 = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5987264555)
    static let riseBlack80 = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.8)
    static let riseVeryLightPink = #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
    static let riseVeryLightPinkTwo = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)
    static let riseDeepSkyBlue = #colorLiteral(red: 0.07058823529, green: 0.5137254902, blue: 1, alpha: 1)
    static let riseTrueGreen = #colorLiteral(red: 0.03137254902, green: 0.5411764706, blue: 0, alpha: 1)
    static let riseTangerine = #colorLiteral(red: 1, green: 0.6, blue: 0, alpha: 1)
    static let riselightPeach = #colorLiteral(red: 1, green: 0.8, blue: 0.7803921569, alpha: 1)
    
    open class var RiseRed: UIColor {
        return UIColor(red: 226/255, green: 30/255, blue: 84/255, alpha: 1)
    }
    
    open class var RiseWordGray: UIColor {
        return UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
    }
    
    open class var RiseTextAndLabelBackGray: UIColor {
        return UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1)
    }
    
    open class var RiseBackGroundGray:UIColor {
        //back ground white
        return UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 0.04)
    }
    
    open class var analysisGressGreen:UIColor {
        return UIColor(displayP3Red: 126/255, green: 211/255, blue: 33/255, alpha: 1)
    }
    
    open class var analysisCalendulaGold:UIColor {
        return UIColor(displayP3Red: 250/255, green: 173/255, blue: 20/255, alpha: 1)
    }
    
    open class var RiseDisableGray:UIColor {
        return UIColor(red: 0, green: 0, blue: 0, alpha: 0.25)
    }
    
    open class var RiseBlack40:UIColor {
        return UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
    }
    
    
}


