//
//  Date+extension.swift
//  ForContract
//
//  Created by Ming Hui Ho on 2020/8/11.
//  Copyright © 2020 LaurenceRedpay20. All rights reserved.
//

import Foundation

extension Date {
    func date2String(dateFormat:String = "yyyy-MM-dd HH:mm:ss") -> String {
        let formatter = DateFormatter()
        formatter.locale = Locale.init(identifier: "zh_TW")
        formatter.dateFormat = dateFormat
        let date = formatter.string(from: self)
        return date
    }
    
//    func daysBetweenDate(toDate: Date) -> Int {
//        let components = Calendar.current.dateComponents([.day], from: self, to: toDate)
//        return components.day ?? 0
//    }
    
    func secBetweenDate() -> Int {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.init(identifier: "zh_TW")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let nowDate = dateFormatter.string(from: Date())
        
        let components = Calendar.current.dateComponents([.second], from: nowDate.string2Date(), to: self)
        return components.second ?? 0
    }
}

extension String {
    func string2Date(dateFormat:String = "yyyy-MM-dd HH:mm:ss") -> Date {
        let formatter = DateFormatter()
        formatter.locale = Locale.init(identifier: "zh_TW")
        formatter.dateFormat = dateFormat
        let date = formatter.date(from: self)
        return date!
    }
    
    func str2Date2Str(dateFormat:String = "yyyy-MM-dd HH:mm:ss") -> String {
        let formatter = DateFormatter()
        formatter.locale = Locale.init(identifier: "zh_TW")
        formatter.dateFormat = dateFormat
        let date = formatter.date(from: self)
        let str = formatter.string(from: date ?? Date())
        return str
    }
}

