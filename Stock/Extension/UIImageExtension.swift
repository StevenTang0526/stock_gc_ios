//
//  UIImageExtension.swift
//  SPAPP
//
//  Created by REDPAY - Laurence on 2020/6/24.
//  Copyright © 2020 REDPAY - Laurence. All rights reserved.
//

import Foundation
import UIKit

public extension UIImage {
    
    enum starType {
        case fill
        case empty
    }
    
    enum ElfImageType {
        case big    // 大圖
        case on     // 小圖
        case off    // 小圖剪影
    }
    
    convenience init?(starType: starType) {
        var name: String
        
        switch starType {
        case .fill:
            name = "icStarFill"
        case .empty:
            name = "icStarEmpty"
        }
        
        self.init(named:name)
    }
    
    convenience init?(Id: String, ElfType: ElfImageType) {
        var name:String
        
        switch ElfType {
        case .big:
            name = "elfBig\(Id)"
        case .on:
            name = "elfOn\(Id)"
        case .off:
            name = "elfOff\(Id)"
        }
        
        self.init(named:name)
    }
    
    convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
    
    func toBase64() -> String? {
        guard let imageData = self.pngData() else { return nil }
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    func resizeImage() -> UIImage {
        
        //prepare constants
        let width = self.size.width
        let height = self.size.height
        let scale = width/height
        
        var sizeChange = CGSize()
        
        if width <= 400 && height <= 400 { //a，圖片寬或者高均小於或等於1280時圖片尺寸保持不變，不改變圖片大小
            return self
        } else if width > 400 || height > 400 {//b,寬或者高大於1280，但是圖片寬度高度比小於或等於2，則將圖片寬或者高取大的等比壓縮至1280
            
            if scale <= 2 && scale >= 1 {
                let changedWidth: CGFloat = 400
                let changedheight: CGFloat = changedWidth / scale
                sizeChange = CGSize(width: changedWidth, height: changedheight)
                
            } else if scale >= 0.5 && scale <= 1 {
                
                let changedheight: CGFloat = 400
                let changedWidth: CGFloat = changedheight * scale
                sizeChange = CGSize(width: changedWidth, height: changedheight)
                
            } else if width > 400 && height > 400 {//寬以及高均大於1280，但是圖片寬高比大於2時，則寬或者高取小的等比壓縮至1280
                
                if scale > 2 {//高的值比較小
                    
                    let changedheight: CGFloat = 400
                    let changedWidth: CGFloat = changedheight * scale
                    sizeChange = CGSize(width: changedWidth, height: changedheight)
                    
                }else if scale < 0.5 {//寬的值比較小
                    
                    let changedWidth:CGFloat = 400
                    let changedheight:CGFloat = changedWidth / scale
                    sizeChange = CGSize(width: changedWidth, height: changedheight)
                    
                }
            } else {//d, 寬或者高，只有一個大於1280，並且寬高比超過2，不改變圖片大小
                return self
            }
        }
        
        UIGraphicsBeginImageContext(sizeChange)
        
        //draw resized image on Context
        self.draw(in: CGRect(x: 0, y: 0, width: sizeChange.width, height: sizeChange.height))
        
        //create UIImage
        let resizedImg = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return resizedImg!
        
    }
    
}
