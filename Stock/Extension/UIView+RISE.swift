//
//  UIView+RISE.swift
//  ForContract
//
//  Created by Ming Hui Ho on 2020/8/26.
//  Copyright © 2020 LaurenceRedpay20. All rights reserved.
//

import UIKit

extension UIView {
    func estimatedHeight(viewWidth: CGFloat) -> CGFloat {
        let largeHeight: CGFloat = 1000
        self.frame = .init(x: 0, y: 0, width: viewWidth, height: largeHeight)
        self.layoutIfNeeded()
        
        return self.systemLayoutSizeFitting(.init(width: viewWidth, height: largeHeight)).height
    }
}
