//
//  UIActivityViewControllerExtension.swift
//  SPAPP
//
//  Created by Ming Hui Ho on 2020/10/20.
//  Copyright © 2020 REDPAY - Laurence. All rights reserved.
//

import UIKit
import PKHUD

extension UIActivityViewController {
    
    convenience init(message: String, image: UIImage, url: URL) {
        
        let items: [Any] = [message, ActivityUrlItemSource(url: url), ActivityImgItemSource(img: image)]
        self.init(activityItems: items, applicationActivities: nil)
        
        excludedActivityTypes = [
            UIActivity.ActivityType.addToReadingList,
            UIActivity.ActivityType.airDrop,
            UIActivity.ActivityType.assignToContact,
            UIActivity.ActivityType.mail,
            UIActivity.ActivityType.message,
            UIActivity.ActivityType.postToTwitter,
            UIActivity.ActivityType.postToFlickr,
            UIActivity.ActivityType.postToVimeo,
            UIActivity.ActivityType.postToWeibo,
            UIActivity.ActivityType.markupAsPDF,
            UIActivity.ActivityType.openInIBooks,
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType("com.apple.reminders.RemindersEditorExtension"),
            UIActivity.ActivityType("com.apple.mobilenotes.SharingExtension")
        ]
        
        completionWithItemsHandler = { activityType, completed, _, error in

            guard completed else { return }

            switch activityType! {
            case UIActivity.ActivityType("jp.naver.line.Share"):
//                SystemStyleAlert.createSuccessAlert(vc: self.parentController!, title: "成功", message: "已經成功分享至Line")
                HUD.flash(.labeledSuccess(title: "成功", subtitle: "已經成功分享至Line"))
            default:
                aPrint("default...")
            }
        }
    }
}
