//
//  UItextFieldExtension.swift
//  SPAPP
//
//  Created by REDPAY - Laurence on 2020/6/1.
//  Copyright © 2020 REDPAY - Laurence. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    
    func setLeftPaddingPoints(_ amount: CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func setRightPaddingPoints(_ amount: CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
    func setLeftPaddingForPlacehold(_ amount: CGFloat) {
        self.bounds.inset(by: UIEdgeInsets(top: 0, left: amount, bottom: 0, right: 0))
    }
    
    func hasBlank() -> Bool {
        for i in self.text ?? "" {
            if i.isWhitespace {
                return true
            }
        }
        return false
    }    
}
