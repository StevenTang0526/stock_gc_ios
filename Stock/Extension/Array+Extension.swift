//
//  Array+Extension.swift
//  ForContract
//
//  Created by Ming Hui Ho on 2020/8/25.
//  Copyright © 2020 LaurenceRedpay20. All rights reserved.
//

import Foundation

extension Array where Element: Comparable {
    
    func containsSameElements(as other: [Element]) -> Bool {
        return self.count == other.count && self.sorted() == other.sorted()
    }

}

