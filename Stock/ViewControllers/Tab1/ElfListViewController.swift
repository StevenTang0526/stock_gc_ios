//
//  ElfIllustratedBookViewController.swift
//  SPAPP
//
//  Created by AndrewPeng on 2020/9/24.
//  Copyright © 2020 REDPAY - Laurence. All rights reserved.
//

import UIKit

class ElfListViewController: UIViewController {
    
    @IBOutlet weak var elfCollView: UICollectionView!
    var elfList: [Elf]?
    var viewModel = ElfListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setViewModel()
        viewModel.getElfList()
        NotificationCenter.default.addObserver(self, selector: #selector(ElfListViewController.reloadCollView), name: NSNotification.Name.riseRefreshElfList, object: nil)
    }
    
    @objc func reloadCollView() {
        elfCollView.reloadData()
    }
    
    func setViewModel() {
        viewModel.delegate = self
        viewModel.parentController = self
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension ElfListViewController: ElfListViewModelDelegate {
    
    func onFetchCompleted(fetchResult: [Elf]) {
        elfList = fetchResult
        DispatchQueue.main.async {
            self.elfCollView.reloadData()
        }
    }
    
    func onFetchFailed() {
        SystemStyleAlert.createAlert(vc: self, title: "網路異常", message: "請檢查您的網路")
    }
    
    
}

extension ElfListViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.elfList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ElfListCollectionViewCell", for: indexPath) as! ElfListCollectionViewCell
        
        let elf = elfList?[indexPath.row]
        
        cell.elfNumLabel.text = "No."+(elf?.id)!
        cell.elfPhotoImg.layer.cornerRadius = 8
        
        if elf?.is_get == true {
            cell.elfNameLabel.text = elf?.name
            cell.elfPhotoImg.image = UIImage(Id: (elf?.id)!, ElfType: .on)
            if elf?.is_haveRead == false {
                cell.elfIsHaveRead.isHidden = false
            } else {
                cell.elfIsHaveRead.isHidden = true
            }
        } else {
            cell.elfPhotoImg.image = UIImage(Id: (elf?.id)!, ElfType: .off)
            cell.elfIsHaveRead.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let elf = elfList?[indexPath.row]
        if elf?.is_get == true {
            _ = CoreDataManager.sharedInstance.update("Elf", predicate: "id ==[c] '\(indexPath.row+1)'", attributeInfo: ["is_haveRead" : true])
            if let vc = storyboard?.instantiateViewController(withIdentifier: "ElfIsGetListViewController") as? ElfIsGetListViewController {
                vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                vc.id = elf?.id
                present(vc, animated: true, completion: nil)
            }
        }
    }
    
}

// MARK: - 設定 CollectionView Cell 與 Cell 之間的間距、距確 Super View 的距離等等
extension ElfListViewController: UICollectionViewDelegateFlowLayout {
    
    /// - Remark: 設定 Collection View 距離 Super View上、下、左、下間的距離
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
    }
    
    /// - Remark: 設定 CollectionViewCell 的寬、高
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let spacing: CGFloat = 16
        let numberOfItemsPerRow: CGFloat = 3
        let spacingBetweenCells: CGFloat = 14
        
        let totalSpacing = (2 * spacing) + ((numberOfItemsPerRow - 1) * spacingBetweenCells)
        
        
        let width = (GlobalValue.screenWidth - totalSpacing)/numberOfItemsPerRow
        
        return CGSize(width: width-1, height: width/0.739)
        //return CGSize(width: GlobalValue.screenWidth*0.28, height: GlobalValue.screenWidth*0.28/0.739)
    }
    
    /// - Remark: 滑動方向為「垂直」的話即「上下」的間距(預設為重直)
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return GlobalValue.screenWidth*0.037
    }
    
    /// - Remark: 滑動方向為「垂直」的話即「左右」的間距(預設為重直)
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return GlobalValue.screenWidth*0.037
    }
    
}
