//
//  FontSizesScaling.swift
//  SPAPP
//
//  Created by AndrewPeng on 2020/9/29.
//  Copyright © 2020 REDPAY - Laurence. All rights reserved.
//

import Foundation
import UIKit


// for label
class ClassLabelSizeClass: UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
        switch UIDevice().currentModel {
        case .iPhoneSE, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE2:
            self.font = self.font.withSize(self.font.pointSize - 2.5)
        case .iPhone6, .iPhone6S, .iPhone7, .iPhone8:
            self.font = self.font.withSize(self.font.pointSize - 1.5)
        case .iPhone6Plus, .iPhone6SPlus, .iPhone7Plus, .iPhone8Plus, .iPhoneXSMax, .iPhoneXR, .iPhone11, .iPhone11ProMax:
            self.font = self.font.withSize(self.font.pointSize + 1)
        case .iPhoneX, .iPhoneXS, .iPhone11Pro:
            self.font = self.font.withSize(self.font.pointSize)
        default:
            self.font = self.font.withSize(self.font.pointSize)
        }
    }
}

//for button
class ClassButtonSizeClass: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        switch UIDevice().currentModel {
        case .iPhoneSE, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE2:
            self.titleLabel?.font = self.titleLabel?.font.withSize((self.titleLabel?.font.pointSize)! - 2.5)
        case .iPhone6, .iPhone6S, .iPhone7, .iPhone8:
            self.titleLabel?.font = self.titleLabel?.font.withSize((self.titleLabel?.font.pointSize)! - 1.5)
        case .iPhone6Plus, .iPhone6SPlus, .iPhone7Plus, .iPhone8Plus, .iPhoneXSMax, .iPhoneXR, .iPhone11, .iPhone11ProMax:
            self.titleLabel?.font = self.titleLabel?.font.withSize((self.titleLabel?.font.pointSize)! + 1)
        case .iPhoneX, .iPhoneXS, .iPhone11Pro:
            self.titleLabel?.font = self.titleLabel?.font.withSize((self.titleLabel?.font.pointSize)!)
        default:
            self.titleLabel?.font = self.titleLabel?.font.withSize((self.titleLabel?.font.pointSize)!)
        }
    }
}

//for textfield
class ClassTextFieldSizeClass: UITextField {
    override func awakeFromNib() {
        super.awakeFromNib()
        switch UIDevice().currentModel {
        case .iPhoneSE, .iPhone5, .iPhone5S, .iPhone5C, .iPhoneSE2:
            self.font = self.font?.withSize(self.font!.pointSize - 2.5)
        case .iPhone6, .iPhone6S, .iPhone7, .iPhone8:
            self.font = self.font?.withSize(self.font!.pointSize - 1.5)
        case .iPhone6Plus, .iPhone6SPlus, .iPhone7Plus, .iPhone8Plus, .iPhoneXSMax, .iPhoneXR, .iPhone11, .iPhone11ProMax:
            self.font = self.font?.withSize(self.font!.pointSize + 1)
        case .iPhoneX, .iPhoneXS, .iPhone11Pro:
            self.font = self.font?.withSize(self.font!.pointSize)
        default:
            self.font = self.font?.withSize(self.font!.pointSize)
        }
    }
}
