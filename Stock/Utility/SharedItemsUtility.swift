//
//  SharedItemsUtility.swift
//  SPAPP
//
//  Created by AndrewPeng on 2020/10/7.
//  Copyright © 2020 REDPAY - Laurence. All rights reserved.
//

import Foundation
import UIKit

class ActivityStringItemSource: NSObject, UIActivityItemSource {
    
    var str: String

    init(str: String) {
        self.str = str
    }
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return ""
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        
        switch activityType {
        case UIActivity.ActivityType.postToFacebook:
            return "Shard to Fb"
        case UIActivity.ActivityType("com.burbn.instagram.shareextension"):
            return "Shard to Instagram"
        case UIActivity.ActivityType("jp.naver.line.Share"):
            return "Shard to LINE"
        default:
            return ""
        }
    }
}

class ActivityUrlItemSource: NSObject, UIActivityItemSource {

    var url: URL!

    init(url: URL) {
        self.url = url
    }

    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return url

    }

    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {

        switch activityType {
        case UIActivity.ActivityType.postToFacebook:
            return url
        case UIActivity.ActivityType("com.burbn.instagram.shareextension"):
            return nil
        case UIActivity.ActivityType("jp.naver.line.Share"):
            return url
        default:
            return url
        }
    }
    
}

class ActivityImgItemSource: NSObject, UIActivityItemSource {

    var img: UIImage!

    init(img: UIImage) {
        self.img = img
    }

    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return img.pngData()
    }

    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {

        switch activityType {
        case UIActivity.ActivityType.postToFacebook:
            return img.pngData()
        case UIActivity.ActivityType("com.burbn.instagram.shareextension"):
            return img.pngData()
        case UIActivity.ActivityType("jp.naver.line.Share"):
            return nil
        default:
            return img.pngData()
        }
    }
    
}
