//
//  ValidDateUtility.swift
//  SPAPP
//
//  Created by AndrewPeng on 2020/8/28.
//  Copyright © 2020 REDPAY - Laurence. All rights reserved.
//

import Foundation
import UIKit

class ValidDateUtility {
    
    static let sharedInstance = ValidDateUtility()
    
    /// 是否為合法的email
    ///
    /// - Parameter email: _
    /// - Returns: _
    func isValidEmail(withEmail email: String) -> Bool {
        let emailRegularExpression = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailNSPredicate = NSPredicate(format:"SELF MATCHES %@", emailRegularExpression)
        return emailNSPredicate.evaluate(with: email)
    }
    
    /// 是否為合法的行動電話
    ///
    /// - Parameter cellPhone: _
    /// - Returns: _
    func isVaildTelPhone(withCellPhone cellPhone: String) -> Bool {
        let cellPhoneRegularExpression = "^09+[0-9]{8}$"
        let phone02 = "^02+[0-9]{8}$"
        let phone03 = "^03+[0-9]{7}$"
        let phone04 = "^04+[0-9]{7}$"
        let phone05 = "^05+[0-9]{7}$"
        let phone06 = "^06+[0-9]{7}$"
        let phone07 = "^07+[0-9]{7}$"
        let phone08 = "^08+([0-9]{7}|[0-9]{6})$"
        
        let cellPhoneNSPredicate = NSPredicate(format:"SELF MATCHES %@", cellPhoneRegularExpression)
        let phone02NSPredicate = NSPredicate(format:"SELF MATCHES %@", phone02)
        let phone03NSPredicate = NSPredicate(format:"SELF MATCHES %@", phone03)
        let phone04NSPredicate = NSPredicate(format:"SELF MATCHES %@", phone04)
        let phone05NSPredicate = NSPredicate(format:"SELF MATCHES %@", phone05)
        let phone06NSPredicate = NSPredicate(format:"SELF MATCHES %@", phone06)
        let phone07NSPredicate = NSPredicate(format:"SELF MATCHES %@", phone07)
        let phone08NSPredicate = NSPredicate(format:"SELF MATCHES %@", phone08)
        
        if cellPhoneNSPredicate.evaluate(with: cellPhone) == true {
            return true
        } else if phone02NSPredicate.evaluate(with: cellPhone) == true {
            return true
        } else if phone03NSPredicate.evaluate(with: cellPhone) == true {
            return true
        } else if phone04NSPredicate.evaluate(with: cellPhone) == true {
            return true
        } else if phone05NSPredicate.evaluate(with: cellPhone) == true {
            return true
        } else if phone06NSPredicate.evaluate(with: cellPhone) == true {
            return true
        } else if phone07NSPredicate.evaluate(with: cellPhone) == true {
            return true
        } else if phone08NSPredicate.evaluate(with: cellPhone) == true {
            return true
        } else {
            return false
        }
    }
    
    /// 是否為合法的室內電話
    ///
    /// - Parameter cellPhone: _
    /// - Returns: _
    func isVaildTaiwanPhone(withCellPhone cellPhone: String) -> Bool {
        let cellPhoneRegularExpression = "(^02+[0-9]{8}$) | (^03+[0-9]{7}$) | (^04+[0-9]{7}$) | (^04+[0-9]{7}$) | (^05+[0-9]{7}$) | (^06+[0-9]{7}$) | (^07+[0-9]{7}$) | (^08+[0-9]{7}$)"
        let cellPhoneNSPredicate = NSPredicate(format:"SELF MATCHES %@", cellPhoneRegularExpression)
        return cellPhoneNSPredicate.evaluate(with: cellPhone)
    }
    
    
    /// 是否為數字
    ///
    /// - Parameter number: _
    /// - Returns: _
    func isNumber(withNumber number: String) -> Bool {
        return !number.isEmpty && number.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
}
